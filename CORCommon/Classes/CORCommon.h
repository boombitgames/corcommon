//
//  CORCommon.h
//  CORCommon
//
//  Created by Siema on 29/05/2022.
//

#ifndef CORCommon_CORCommon_h
#define CORCommon_CORCommon_h

#include "UnityInterface.h"

// Converts C style string to NSString
#define GetStringParam( _x_ ) ( _x_ != NULL ) ? [NSString stringWithUTF8String:_x_] : [NSString stringWithUTF8String:""]

// Converts C style string to NSString as long as it isnt empty
#define GetStringParamOrNil( _x_ ) ( _x_ != NULL && strlen( _x_ ) ) ? [NSString stringWithUTF8String:_x_] : nil

// Converts NSString to C style string by way of copy (Mono will free it)
#define MakeStringCopy( _x_ ) ( _x_ != NULL && [_x_ isKindOfClass:[NSString class]] ) ? strdup( [_x_ UTF8String] ) : NULL

// Basic delegate types for native proxy implementation
typedef void (*BasicDelegate)(void);
typedef void (*StringDelegate)(const char*);
typedef void (*DoubleStringDelegate)(const char*, const char*);

#endif /* CORCommon_CORCommon_h */
