//
//  UnityInterface.mm
//  CORCommon
//
//  Created by Siema on 31/05/2022.
//

#ifndef COREDIAN_UNITY_BUILD

#include "UnityInterface.h"

#ifdef __cplusplus
extern "C" {
#endif

void UnitySendMessage(const char* obj, const char* method, const char* msg) {}
void UnityPause(int pause) {}

UIViewController* UnityGetGLViewController(void) { return nil; }
UIWindow* UnityGetMainWindow(void) { return nil; }

#ifdef __cplusplus
}
#endif

#endif
