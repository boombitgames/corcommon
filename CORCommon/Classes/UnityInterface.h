//
//  UnityInterface.h
//  CORCommon
//
//  Forward declaration of Unity calls defined in the main app target
//
//  Created by Siema on 29/05/2022.
//

#ifndef CORCommon_UnityInterface_h
#define CORCommon_UnityInterface_h

#ifdef __OBJC__
@class UIViewController;
@class UIWindow;
#else
typedef struct objc_object UIViewController;
typedef struct objc_object UIWindow;
#endif

#ifdef __cplusplus
extern "C" {
#endif

extern void UnitySendMessage(const char* obj, const char* method, const char* msg);
extern void UnityPause(int pause);

extern UIViewController* UnityGetGLViewController(void);
extern UIWindow* UnityGetMainWindow(void);

#ifdef __cplusplus
}
#endif

#endif /* CORCommon_UnityInterface_h */
